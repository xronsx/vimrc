# Vimrc by xronsx

![Alt text](images/Captura desde 2022-08-26 11-35-30.png?raw=true "screenshot1")
![Alt text](images/Captura desde 2022-08-26 11-36-26.png?raw=true "screenshot2")


## neovim configuration

Install neovim

```sh
sudo apt install fd-find
sudo apt-get install ripgrep
sudo add-apt-repository ppa:neovim-ppa/stable
sudo apt-get update
sudo apt-get install neovim
```

Install python support for neovim

```sh
sudo apt-get install python-neovim
sudo apt-get install python3-neovim
```

After cloning the repository, create in the ~/root (or user) directory the following symbolic link:

```sh
sudo ln -s /path/to/.vimrc-file/.vimrc /root/
```

Don't forget to place the nvim/ directory and .vimrc file inside of .config/ directory.

```
└── user_directory/
    ├── .vimrc (symbolic link)
    ├── .tmux.conf (symbolic link)
    └── .config/
        ├── nvim/
        └── .vimrc
        └── .tmux.conf
        └── .flake8
```

## ctags configuration

Install ctags

```sh
sudo apt install exuberant-ctags
```

After install ctags, in your app source code run the following:

Ref: https://www.youtube.com/watch?v=zl4q6dScneY&list=FLLXnMRrGiaZ6F-5EM2v-ooA&index=1&t=215s

```sh
ctags -R ./
```

## tmux configuration

Install tmux

```sh
apt install tmux
```

Create in the ~/root (or user) directory the following symbolic link:

```sh
sudo ln -s /path/to/.tmux.conf-file/.tmux.conf /root/
```

Once you start tmux at the first time, Hit prefix + I to fetch the plugin and source it. You should now be able to use all the plugin.


## enable linter and fixer plugins

To enable linter plugin (flake8) it is necesary to install the following package:

Ref: https://flake8.pycqa.org/en/latest/

```sh
pip install flake8
```

You can test it like:

```sh
flake8 demo.py
```

To enable fixer plugin (black) it is necesary to install the following package:

Ref: https://github.com/psf/black

```sh
pip install black
```

You can test it like:

```sh
black demo.py
```

## Enable coc with pyright

Ref: https://www.youtube.com/watch?v=i_LhEHkDKs4&t=581s

Install nodejs>=12.12

```sh
curl -sL install-node.vercel.app/lts | bash
```

Install coc plugins

```sh
CocIsnstall coc-pyright
CocIsnstall coc-snippets
CocIsnstall coc-word
```

## Enable isort plugin 

Ref: https://github.com/stsewd/isort.nvim

Install isort 

```sh
pip install isort
```



# Optionals: 

## Custom pyright configuration per project

Copy the pyrightconfig.json file into root project folder

```sh
sudo cp pyrightconfig.json /root/project/forder/
```


# Install fonts for gnome terminal

```sh
This can be installed by snap library
https://github.com/FontManager/font-manager

https://github.com/adidenko/powerline
```

# Install prompt StarShip


Install the latest version for your system:
```sh
curl -sS https://starship.rs/install.sh | sh
```

Add the following to the end of ~/.bashrc
```sh
eval "$(starship init bash)"
```

# Coc json configuration


Install required extensions
```sh
:CocInstall coc-json coc-tsserver
```

:CocConfig and Write the following configurations
```sh
{
 "diagnostic.messageTarget": "float",
 "diagnostic.checkCurrentLine": true,
 "diagnostic.virtualText": true,
 "diagnostic.virtualTextCurrentLineOnly": false,
 "diagnostic.virtualTextPrefix": "<-",
 "typescript.suggest.autoImports": true,
 "javascript.suggest.autoImports": true
}
```
